INSERT INTO public.authority (id, authority) VALUES (1, 'ROLE_ADMIN');
INSERT INTO public.account (id, password, username) VALUES (1, '$2a$11$IzHZYzJNySOWlvza6lfpYuXa4DcQGlm6g45ujxiOasfi2kNk/droG', 'admin');
INSERT INTO public.account_authorities (account_id, authorities_id) VALUES (1, 1);