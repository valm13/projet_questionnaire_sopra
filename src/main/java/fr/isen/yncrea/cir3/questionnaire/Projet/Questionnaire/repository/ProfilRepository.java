package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Profil;

@Repository
public interface ProfilRepository extends JpaRepository<Profil, Long> {

}
