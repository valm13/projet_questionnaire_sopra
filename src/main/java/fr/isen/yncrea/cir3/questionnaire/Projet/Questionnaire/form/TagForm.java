package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class TagForm {
    private Long id;
    @NotEmpty
    private String nom;
}
