package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.AccountRepository;

@Service
public class DbUserDetailsService implements UserDetailsService {

	@Autowired
	private AccountRepository accounts;
	
	public UserDetails loadUserByUsername(String login) {
		UserDetails user = accounts.findByUsername(login);
		if(user == null)
			throw new UsernameNotFoundException(login);
		return user;
	}

}
