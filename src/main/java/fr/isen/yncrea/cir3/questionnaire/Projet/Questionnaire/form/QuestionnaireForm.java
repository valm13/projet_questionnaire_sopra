package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form;


import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Range;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionnaireForm {
	private Long id;
	
//	@NotEmpty
	private String nom;
	
//	@NotEmpty
	private String description;
	
//	@Range(min=1,max=15)
	private int nombre_question;

	
}
