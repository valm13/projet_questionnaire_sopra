package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Question;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long>{

	Question findByNumero(Long numero);

}
