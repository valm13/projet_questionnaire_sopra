package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.controller;

import java.util.ArrayList;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Passage;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Question;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Questionnaire;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Tag;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form.AddPassageForm;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form.QuestionnaireEditForm;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form.ReponseQuestionForm;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.PassageRepository;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.ProfilRepository;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.QuestionnaireRepository;
import lombok.Getter;
import lombok.Setter;


@Controller
@RequestMapping("/passage")
public class PassageController {
	@Autowired
	private QuestionnaireRepository questionnaires;
	
	@Autowired
	private ProfilRepository profils;
	
	@Autowired
	private PassageRepository passages;
	
	@GetMapping("/add")
	public String add(Model model) {
		AddPassageForm form = new AddPassageForm();
		
		model.addAttribute("passageForm", form);
		model.addAttribute("questionnaires", questionnaires.findByEtat("ready"));
		model.addAttribute("profils", profils.findAll());
		return "passage-add";
	}
	
	@PostMapping("/add")
	public String addForm(@Valid @ModelAttribute("passage") AddPassageForm form, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute("passageForm", form);
			model.addAttribute("questionnaires", questionnaires.findAll());
			model.addAttribute("profils", profils.findAll());
			return "passage-add";
		}
		
		if(questionnaires.findById(form.getId_questionnaire()).isPresent() && profils.findById(form.getId_profil()).isPresent())
		{
			Passage p = new Passage();
			
			p.setDate_test(new Date(System.currentTimeMillis()));
			p.setNom(form.getNom());
			p.setPrenom(form.getPrenom());
			p.setQuestionnaire(questionnaires.findById(form.getId_questionnaire()).get());
			p.setProfil(profils.findById(form.getId_profil()).get());
			p.setQuestion_actuelle(1);
			p.setEtat("Just created");
			p.setAnonyme(0);
			
			passages.save(p);
			return "redirect:/passage/qr/"+p.getId();
		}
		else
			return "redirect:/passage/add";
		
		
		
	}
	@GetMapping("/qr/{id}")
	public String qr(Model model, @PathVariable Long id) {
		
		Passage p = passages.findById(id).get();
		model.addAttribute("passage", p);
		String link = new String("http://192.168.43.221/passage/go/"+p.getId());
		
		model.addAttribute("link", link);
		return "passage-qr";
	}
	
	@GetMapping("/go/{id}")
	public String go(Model model, @PathVariable Long id) {
		
		if(!passages.findById(id).isPresent())
			return "redirect:/passage/qr/"+id;
		Passage p = passages.findById(id).get();
		Question q = p.getQuestionnaire().getQuestionByNumero(p.getQuestion_actuelle());
		if(q != null)
		{
			String strTags = new String("(");
			int index = 0;
			for(Tag t : q.getTags())
			{
				strTags = strTags.concat(t.getNom());
				if(!(index == q.getTags().size()-1))
					strTags = strTags.concat(", ");
				else
					strTags = strTags.concat(")");
				index++;
			}
			
			ArrayList<Reponse> reponses = new ArrayList<Reponse>();
			int value = 1;
			for(String reponse : q.getReponses()) {
				reponses.add(new Reponse(reponse,value));
				value++;
			}
			p.setEtat("Question "+p.getQuestion_actuelle());
			ReponseQuestionForm form = new ReponseQuestionForm();
			model.addAttribute("passage", p);
			model.addAttribute("question", q);
			model.addAttribute("reponses", reponses);
			model.addAttribute("tags", strTags);
			model.addAttribute("form",form);
			passages.save(p);
			return "passage-go";
		}
		else return "redirect:/passage/qr/"+p.getId();
	}
	
	@PostMapping("/go/{id}")
	public String goForm(@ModelAttribute("passageForm") ReponseQuestionForm form, @PathVariable Long id) {
		if(passages.findById(id).isPresent()) {
			Passage p = passages.findById(id).get();
			p.getReponses().add(new Long(form.getNum_reponse()));
			Long question_suivante = new Long(p.getQuestion_actuelle() + 1);
			
			if(question_suivante.intValue() > p.getQuestionnaire().getNombre_question())
			{
				passages.save(p);
				return "redirect:/passage/go/result/"+id;
			}
				
			else
			{
				// Si la question suivante existe, on continue
				p.setQuestion_actuelle(question_suivante.intValue());
				passages.save(p);
				return "redirect:/passage/go/"+id;
			}
		}	
		else
			return "passage-error";
	}
	
	@GetMapping("/go/result/{id}")
	public String result(Model model, @PathVariable Long id) {
		
		if(!passages.findById(id).isPresent())
			return "redirect:/passage/qr/"+id;
		
		Passage p = passages.findById(id).get();
		p.setNote(p.calculNote());
		p.setEtat("Terminé");
		passages.save(p);
		model.addAttribute("passage",p);
		return "passage-go-result";
	
	}
	
	@GetMapping("/list")
	public String list(Model model) {

		model.addAttribute("passages",passages.findByAnonyme(0));
		return "passage-list";
	
	}
	
	@Getter
	@Setter
	class Reponse{
		String s;
		int valeur;
		public Reponse(String s, int valeur)
		{
			this.s = s;
			this.valeur = valeur;
		}
	}
	
	@GetMapping("/anonymise/{id}")
	public String anonymisePassage(@PathVariable Long id, Model model) {
		
		if(passages.findById(id).isPresent())
		{
			Passage p = passages.findById(id).get();
			p.setNom(null);
			p.setPrenom(null);
			p.setAnonyme(1);
			passages.save(p);
		}
		return "redirect:/passage/list/";
	}
	
	@GetMapping("/resultatadmin/{id}")
	public String resultats(@PathVariable Long id,Model model){
		Passage p = passages.findById(id).get();
		
		
		model.addAttribute("passage",p);
		model.addAttribute("AllQuestionsCouleursRep",couleurReponses(p));
		return "passage-resultat-admin";
	}
	private int[][] couleurReponses(Passage p)
	{
		int[][] array = new int[p.getQuestionnaire().getNombre_question()][4];
		Questionnaire questionnaire = p.getQuestionnaire();
		for(int i = 0; i < array.length; i++)
		{
			array[i] = calculCouleursQuestion(questionnaire.getQuestionByNumero(i + 1), p, i);
		}
		
		return array;
	}
	
	// Calcul les couleurs de chaques réponses d'un passage : 0 = Pas de couleur, 1 = Vert, 2 = Rouge
	private int[] calculCouleursQuestion(Question q, Passage p, int index)
	{
		int[] arrayCouleursReponses = new int[4];
		int bonneRep = q.getBonne_reponse();
		int maRep = p.getReponses().get(index).intValue();
		
		// On met la bonne réponse en vert à la case de  cette bonne réponse donc bonneRep - 1 car notre tableau débute à 0
		arrayCouleursReponses[bonneRep - 1] = 1;
		
		// Si on n'a pas la bonne réponse on met la nôtre en rouge
		if(bonneRep != maRep)
			arrayCouleursReponses[maRep - 1] = 2;
			
			
		return arrayCouleursReponses;
		
		
	}
	
	@GetMapping("/delete/{id}")
	public String deletePassage(@PathVariable Long id, Model model) {
		
		if(passages.findById(id).isPresent())
		{
			Passage p = passages.findById(id).get();
			passages.delete(p);
		}
		return "redirect:/passage/list/";
	}
	
}