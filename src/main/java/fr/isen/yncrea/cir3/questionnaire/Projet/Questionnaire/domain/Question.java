package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "question")
public class Question {
	
	@Id
	@Column
	@GeneratedValue
	private Long id;

	@Column
	private String question;
	
	@Column
	private int temps;
	
	@Column
	private int numero;
	
	@Column
	private ArrayList<String> reponses = new ArrayList<String>();
	
	@ManyToMany//(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<Tag> tags = new ArrayList<>();
	
	@Column
	private int bonne_reponse;


	public String getReponses(int i){
		return reponses.get(i);
	}
//	@ManyToOne
//	private Questionnaire questionnaire;
}
