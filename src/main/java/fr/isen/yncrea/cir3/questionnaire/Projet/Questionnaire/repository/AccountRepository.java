package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {
	public Account findByUsername(String username);

}
