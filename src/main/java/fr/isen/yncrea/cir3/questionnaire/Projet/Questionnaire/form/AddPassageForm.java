package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddPassageForm {

	@Id
	@Column
	@GeneratedValue
	private Long id;

	@NotEmpty
	private String nom;
	
	@NotEmpty
	private String prenom;
	
	private Long id_profil;
	
	private Long id_questionnaire;


}
