package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NoteAndTag {
	
	private String tag; 
	private double note;
	private double moy;
	
	public NoteAndTag(String tag, double note, double moy) {
		this.tag = tag;
		this.note = note;
		this.moy = moy;
	}

}
