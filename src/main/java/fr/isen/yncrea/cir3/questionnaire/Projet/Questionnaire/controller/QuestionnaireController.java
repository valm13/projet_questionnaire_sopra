package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Question;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Questionnaire;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form.QuestionForm;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form.QuestionnaireEditForm;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form.QuestionnaireForm;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.QuestionRepository;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.QuestionnaireRepository;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.TagRepository;


@Controller
@RequestMapping("/questionnaire")
public class QuestionnaireController {

	@Autowired
	private QuestionnaireRepository questionnaires;
	
	@Autowired
	private QuestionRepository questions;
	
	@Autowired
	private TagRepository tags;
	


	@GetMapping({"","/list"})
	public String list(Model model, @PageableDefault(page=0,size=5)Pageable pageable) {
		model.addAttribute("questionnaires", questionnaires.findAll(pageable));
		return "questionnaire-list";
	}

	@GetMapping("/add")
	public String addQuestionnaire(Model model) {
		
		QuestionnaireForm form = new QuestionnaireForm();
		// Default value
		form.setNombre_question(1);
		model.addAttribute("questionnaire", form);
		return "questionnaire-add";
	}
	
	@PostMapping("/add")
	public String addQuestionnaireForm(@Valid @ModelAttribute("questionnaire") QuestionnaireForm form, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute("questionnaire", form);
			return "questionnaire-add";
		}
		
		Questionnaire q = new Questionnaire();
		
		// Si on était en mode édition on récupère le formulaire actuellement en base
		if(form.getId() != null)
			 q = questionnaires.findById(form.getId()).get();
		// Le nombre de question ne sera pas modifiable, de cette manière en tout cas
		else
			q.setNombre_question(form.getNombre_question());
		
		// Puis on le crée ou modifie
		q.setEtat("creation");
		q.setNom(form.getNom());
		String description = form.getDescription();
		q.setDescription(description);
		q.setNombre_question(form.getNombre_question());
		q.setDate_creation(new Date(System.currentTimeMillis()));
		
		
		questionnaires.save(q);

		return "redirect:/questionnaire/add/"+q.getId()+"/question/add";
	}
	
	@GetMapping("/add/{id}/question/add")
	public String addQuestion(@PathVariable Long id, @PathVariable(required=false) Long id_question, Model model) {
		if(questionnaires.findById(id).isPresent())
		{
			QuestionForm form = new QuestionForm();
			// Valeur par défaut
			form.setTemps(60);
			form.setBonne_reponse(1);
			model.addAttribute("question", form);
			model.addAttribute("questionnaire", questionnaires.findById(id).get());
			model.addAttribute("tags", tags.findAll());
			return "question-add";
		
		}
		return "redirect:/questionnaire";
	}
	
	@PostMapping("/add/{id}/question/add")
	public String addQuestionForm(@PathVariable Long id, @Valid @ModelAttribute("question") QuestionForm form, BindingResult result, Model model) {
		
		if (questionnaires.findById(id).isPresent()) 
		{
			Questionnaire questionnaire = questionnaires.findById(id).get();
			if (result.hasErrors()) {
				model.addAttribute("question", form);
				model.addAttribute("questionnaire", questionnaires.findById(id).get());
				model.addAttribute("tags", tags.findAll());
				return "question-add";
			}
			
			List<Question> qs = questionnaire.getQuestions();
			
			Question q = new Question();
			
			q.setBonne_reponse(form.getBonne_reponse());
			// Le numéro de la question en création est le nombre de questions deja présentes + 1
			q.setNumero(questionnaire.getQuestions().size()+ 1);
			q.setBonne_reponse(form.getBonne_reponse());
			q.setQuestion(form.getQuestion());
			q.setTemps(form.getTemps());
			q.setTags(form.getTags().stream().map(e -> tags.findById(e).get()).collect(Collectors.toList()));

			List<String> array = q.getReponses();
			array.add(0, form.getReponse1());
			array.add(1, form.getReponse2());
			array.add(2, form.getReponse3());
			array.add(3, form.getReponse4());
			
			// On ajoute la question au questionnaire puis on sauvegarde le questionnaire
			qs.add(q);
			
			
			System.out.println(questionnaire.getQuestions().size());
			if(questionnaire.getQuestions().size() < questionnaire.getNombre_question())
				return "redirect:/questionnaire/add/{id}/question/add";
			
			questionnaire.setEtat("ready");
			questionnaires.save(questionnaire);
			
		}
		return "redirect:/questionnaire";

	}
//	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {
		if (questionnaires.findById(id).isPresent())
			questionnaires.deleteById(id);
		return "redirect:/questionnaire";
	}
	
	
	@GetMapping({"view/{id}/home","/view/{id}"})
	public String view(@PathVariable Long id, Model model) {
		if(questionnaires.findById(id).get().getEtat().equals("creation"))
			return "redirect:/questionnaire/add/"+id+"/question/add";
		model.addAttribute("questionnaire", questionnaires.findById(id).get());
		model.addAttribute("questions", questionnaires.findById(id).get().getQuestions());
		System.out.println(questionnaires.findById(id).get().getEtat());
		int sommeTemps = 0;
		for(Question q : questionnaires.findById(id).get().getQuestions())
		{
			sommeTemps+= q.getTemps();
		}
		model.addAttribute("tempsMax", sommeTemps);
		return "questionnaire-view";
	}
	
	@GetMapping("view/{id_questionnaire}/question/view/{numero}")
	public String view(@PathVariable Long id_questionnaire, @PathVariable Long numero, Model model) {
		if(questionnaires.findById(id_questionnaire).isPresent())
		{
			Questionnaire questionnaire = questionnaires.findById(id_questionnaire).get();
			model.addAttribute("questionnaire", questionnaire);
			Question q  = questionnaire.getQuestionByNumero(numero.intValue());
			
			if(q != null)
			{
				model.addAttribute("question", q);
				return "question-view";
			}
			else
				return "redirect:/questionnaire/view/"+id_questionnaire;
				
		}
		else
			return "redirect:/questionnaire/list/";	
		
	}
	
	@GetMapping("/edit/{id}")
	public String editQuestionnaire(@PathVariable Long id, Model model) {
		
		QuestionnaireEditForm form = new QuestionnaireEditForm();
		model.addAttribute("questionnaire", form);

		Questionnaire q = questionnaires.findById(id).get();
		form.setId(q.getId());
		form.setNom(q.getNom());
		form.setDescription(q.getDescription());
		
		return "questionnaire-edit";
	}
	
	@PostMapping("/edit")
	public String editQuestionnaireForm(@Valid @ModelAttribute("questionnaire") QuestionnaireEditForm form, BindingResult result, Model model) {
		
		Questionnaire q = new Questionnaire();
		
		if(form.getId() != null)
		{
			if(questionnaires.findById(form.getId()).isPresent())
			{
				q = questionnaires.findById(form.getId()).get();
				q.setDescription(form.getDescription());
				q.setNom(form.getNom());
				questionnaires.save(q);
			}
			
		}

		return "redirect:/questionnaire/view/"+q.getId();
	}
	
	@GetMapping("/view/{id_questionnaire}/question/edit/{numero}")
	public String editQuestion(@PathVariable Long id_questionnaire, @PathVariable Long numero,Model model){
		QuestionForm form = new QuestionForm();
		Questionnaire questionnaire = questionnaires.findById(id_questionnaire).get();

		model.addAttribute("questionnaire", questionnaire);
		model.addAttribute("question",form);
		model.addAttribute("tags", tags.findAll());

		Question q = questionnaire.getQuestionByNumero(numero.intValue());

		form.setId(q.getId());
		form.setQuestion(q.getQuestion());
		form.setReponse1(q.getReponses(0));
		form.setReponse2(q.getReponses(1));
		form.setReponse3(q.getReponses(2));
		form.setReponse4(q.getReponses(3));
		form.setBonne_reponse(q.getBonne_reponse());

		form.setTemps(q.getTemps());


		return "question-edit";
	}

	@PostMapping("view/{id_questionnaire}/question/edit")
	public String editQuestionForm(@Valid @ModelAttribute("question") QuestionForm form,@PathVariable Long id_questionnaire,BindingResult result,Model model){
		Question q = questions.findById(form.getId()).get();

		if(form.getId() != null)
		{
			q.setQuestion(form.getQuestion());
			ArrayList<String> array = new ArrayList<>();

			array.add(form.getReponse1());
			array.add(form.getReponse2());
			array.add(form.getReponse3());
			array.add(form.getReponse4());

			q.setBonne_reponse(form.getBonne_reponse());
			q.setReponses(array);

			q.setTemps(form.getTemps());

			q.setTags(form.getTags().stream().map(e -> tags.findById(e).get()).collect(Collectors.toList()));

			questions.save(q);
		}

		return "redirect:/questionnaire/view/"+id_questionnaire+"/question/view/"+q.getNumero();
	}

}
