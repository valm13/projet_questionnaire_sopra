package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.controller.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.NoteAndTag;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Passage;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Question;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Tag;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.PassageRepository;

@RestController
@RequestMapping("/api/passage")
public class PassageRestController {
	
		@Autowired
		private PassageRepository passages;
		
		
		@GetMapping("/search/mypassage/{id}")
//		@JsonView
		public List<NoteAndTag> getMyData(@PathVariable(required = true) Long id)
		{
			
			Passage pBase = passages.findById(id).get();
			Set<Tag> tags = pBase.getQuestionnaire().getAllTags();
			List<NoteAndTag> listeNoteAndTag = new ArrayList<NoteAndTag>();
			
			// On commence par calculer les moyennes
			
			// Tout les passages anonymisés ayant le même profil
			List<Passage> allPassageWithSameProfil =  new ArrayList<Passage>();
			
			// Pour chaque personne ayant passé ce questionnaire et qui ont leur nom à null, on ajoute ce passage dans un tableau
			for(Passage p : pBase.getQuestionnaire().getPassages())
			{
				// Si ils ont le même profil et que le profil actuel est anonyme on l'ajoute au tableau.
				if(pBase.getProfil().equals(p.getProfil()) && p.getNom() == null)
				{
					allPassageWithSameProfil.add(p);
				}
			}

			double notesMoyennes[] = new double[pBase.getQuestionnaire().getAllTags().size()];
			// Puis on calcul la note pour chaque tag de chaque passage anonymisés que l'on additionne à celle de noteMoyennes :
			int index;
			for(Passage p : allPassageWithSameProfil)
			{
				index = 0;
				for(Tag t : tags)
				{
					// On Additionne dans noteMoyenne, la note de tel profil à tel tag divisé par le nombre de profil
					notesMoyennes[index] += calculNoteReponsesFromTag(t, p) / allPassageWithSameProfil.size() ;
					index++;
				}
				
			}
			
			
			index = 0;
			for(Tag t : tags)
			{
//				for(double note : notesMoyennes)
//					System.out.println(note);
//				System.out.println("---");
				listeNoteAndTag.add(new NoteAndTag(t.getNom(), calculNoteReponsesFromTag(t, pBase),notesMoyennes[index]));
				index++;
			}
			for(NoteAndTag nt : listeNoteAndTag)
				System.out.println(nt);
			return listeNoteAndTag;

		}
				
		private double calculNoteReponsesFromTag(Tag t, Passage p)
		{
			int nbBonneRep = 0;
			int noteMax = 5;
			// On récupère les questions portant ce Tag et le questionnaire du passage
			List<Question> listQuestions = p.findQuestionsByTagAndQuestionnaire(t,p.getQuestionnaire());

			if(listQuestions.size() > 0)
			{
				for(Question q : listQuestions)
				{
					//  On vérifie si la réponse est bonne
					if(q.getBonne_reponse() == p.getReponses().get(q.getNumero() - 1))
					{
						nbBonneRep++;
					}
						
				}
				double note = noteMax * (double)nbBonneRep / listQuestions.size();
				//System.out.println("Tag : "+t.getNom()+", nb Bonne rep : "+nbBonneRep+", nbQuestionAvecCeTag : "+listQuestions.size()+ "note = "+note);

				return note;
			}
			else
				return -1; // Pour remarquer s'il y a une erreur
		}
}
