package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.security.core.userdetails.UserDetails;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "account")
public class Account implements UserDetails{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6407893062337433670L;

	@Id @Column
	@GeneratedValue
	private Long id;
	
	@Column(length = 100)
	private String username;
	
	@Column(length = 100)
	private String password;
	
	@ManyToMany(fetch=FetchType.EAGER)
	private Collection<Authority> authorities;

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}