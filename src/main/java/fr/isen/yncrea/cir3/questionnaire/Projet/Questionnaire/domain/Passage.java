package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Passage {
	
	@Id
	@Column
	@GeneratedValue
	private Long id;

	@Column
	private String nom;
	
	@Column
	private String prenom;
	
	@ManyToOne
	@JoinColumn(name = "profil_id",
				foreignKey = @ForeignKey(name = "PROFIL_ID_FK"))
	private Profil profil;
	
	@ManyToOne
	@JoinColumn(name = "questionnaire_id",
				foreignKey = @ForeignKey(name = "QUESTIONNAIRE_ID_FK"))
	private Questionnaire questionnaire;
	
	@Column(nullable = false)
	private Date date_test;
	
	@Column(nullable = false)
	private ArrayList<Long> reponses = new ArrayList<Long>();
	
	@Column
	private int note;
	
	@Column
	private int anonyme;
	
	@Column
	private String etat;
	
	@Column
	private int question_actuelle;
	
	public int calculNote()
	{
		int nbBonneReponses = 0;
		List<Question> liste_question = questionnaire.getQuestions();
		int index = 0;
		for(Question q : liste_question)
		{
			if(q.getBonne_reponse() == reponses.get(index))
				nbBonneReponses++;
			index++;
		}
		return nbBonneReponses;
	}
	
	public List<Question> findQuestionsByTagAndQuestionnaire(Tag t, Questionnaire questionnaire)
	{
		List<Question> allQuestions = questionnaire.getQuestions();
		List<Question> questionsAyantTag = new ArrayList<Question>();
		
		for(Question q : allQuestions)
		{
			if(q.getTags().contains(t))
				questionsAyantTag.add(q);
		}
		return questionsAyantTag;	
	}

}
