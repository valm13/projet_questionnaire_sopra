package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class User {

	@Id
	@Column
	@GeneratedValue
	private Long id;
	
	@Column
	private String nom;
	
	@Column
	private String prenom;
	
	@Column
	private Long id_questionnaire;
	
	@Column
	private Date date_lancement;
	
	@Column
	private Date date_rendu;
	
	@Column
	private ArrayList<Long> id_reponses = new ArrayList<Long>();
}