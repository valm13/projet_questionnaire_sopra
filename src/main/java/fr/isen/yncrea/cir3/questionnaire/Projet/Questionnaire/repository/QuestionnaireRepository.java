package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Questionnaire;

@Repository
public interface QuestionnaireRepository extends JpaRepository<Questionnaire, Long>{
	
	List<Questionnaire> findByEtat(String etat);
}
