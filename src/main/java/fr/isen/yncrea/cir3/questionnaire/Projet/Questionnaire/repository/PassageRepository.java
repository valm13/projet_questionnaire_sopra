package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Passage;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Profil;

@Repository
public interface PassageRepository extends JpaRepository<Passage, Long> {
	
	List<Passage> findByAnonyme(int b);
	List<Passage> findByAnonymeAndProfil(int b, Profil p);
}
