package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReponseQuestionForm {

	private int num_reponse;
}
