package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

	@GetMapping("/")
	public String welcome(Model model) {
		model.addAttribute("message", "Bienvenue sur le créateur de questionnaire de Sopra Steria");
		return "welcome";
	}
	
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
}
