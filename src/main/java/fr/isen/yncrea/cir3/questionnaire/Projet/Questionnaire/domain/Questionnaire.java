package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "questionnaire")
public class Questionnaire {
	
	@Id
	@Column
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String nom;
	
	@Column(nullable = false, length=1200)
	private String description;
	
	@Column(nullable = false)
	private int nombre_question;
	
	@Column
	private String etat;
	
	@Column
	private Date date_creation;
	
	@OneToMany(/*mappedBy = "questionnaire",*/ cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Question> questions = new ArrayList<>();
	
	@OneToMany(mappedBy = "questionnaire", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Passage> passages = new ArrayList<>();
	
	public Question getQuestionByNumero(int numero)
	{
		for(Question question : questions)
		{
			if(question.getNumero() == numero)
				return question;
		}
		return null;
	}
	
	public Set<Tag> getAllTags()
	{
		Set<Tag> tags= new HashSet<Tag>();
		for(Question question : questions)
		{
			for(Tag t : question.getTags())
			{
				// 
				if(!tags.contains(t))
					tags.add(t);
			}
		}
		return tags;
	}
}
