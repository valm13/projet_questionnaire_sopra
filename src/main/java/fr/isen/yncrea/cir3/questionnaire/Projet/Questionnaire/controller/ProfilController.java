package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Profil;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form.ProfilForm;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.ProfilRepository;;

@Controller
@RequestMapping("/profil")
public class ProfilController {

    @Autowired
    private ProfilRepository profils;

    @GetMapping({"","/list"})
    public String list(Model model){
        model.addAttribute("profils", profils.findAll());
        return "profil-list";
    }

    @GetMapping({"/add","edit/{id}"})
    public String add(@PathVariable(required=false) Long id, Model model){
        ProfilForm form = new ProfilForm();
        model.addAttribute("profil",form);
        if(id != null)
        {
            Profil p = profils.findById(id).get();

            form.setId(p.getId());
            form.setNom(p.getNom());
        }
        return "profil-add";
    }

    @PostMapping("/add")
    public String addForm(ProfilForm form, BindingResult result, Model model){
        if(result.hasErrors())
        {
            model.addAttribute("profil",form);
            return "profil-add";
        }
        Profil p = new Profil();
        if(form.getId() != null)
        	if(profils.findById(form.getId()).isPresent())
        		p = profils.findById(form.getId()).get();
        
        p.setNom(form.getNom());
        profils.save(p);

        return "redirect:/profil/list";
    }
}
