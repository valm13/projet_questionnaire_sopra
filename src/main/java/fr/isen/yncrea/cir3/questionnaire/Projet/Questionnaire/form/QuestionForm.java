package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionForm {


	private Long id;
	
//	@Range(min=30, max=120)
	private int temps;
	
//	@Range(min=1, max=4)
	private int bonne_reponse;

//	@Length(min=5, max=100)
	private String question;

//	@Length(min=5, max=100)
	private String reponse1;

//	@Length(min=5, max=100)
	private String reponse2;

//	@Length(min=5, max=100)
	private String reponse3;

//	@Length(min=5, max=100)
	private String reponse4;
	
	@NotEmpty
	private List<Long> tags;
	


}
