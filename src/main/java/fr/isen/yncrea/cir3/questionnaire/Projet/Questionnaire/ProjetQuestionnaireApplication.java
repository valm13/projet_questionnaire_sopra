package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetQuestionnaireApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetQuestionnaireApplication.class, args);
	}

}
