package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Profil {
	
	@Id
	@Column
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String nom;

}
