package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

}
