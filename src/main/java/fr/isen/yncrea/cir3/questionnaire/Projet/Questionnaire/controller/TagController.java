package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.controller;

import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.domain.Tag;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form.TagForm;
import fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tag")
public class TagController {

    @Autowired
    private TagRepository tags;

    @GetMapping({"","/list"})
    public String list(Model model){
        model.addAttribute("tags", tags.findAll());
        return "tag-list";
    }

    @GetMapping({"/add","edit/{id}"})
    public String add(@PathVariable(required=false) Long id, Model model){
        TagForm form = new TagForm();
        model.addAttribute("tag",form);

        if(id != null)
        {
            Tag t = tags.findById(id).get();

            form.setId(t.getId());
            form.setNom(t.getNom());
        }
        return "tag-add";
    }

    @PostMapping("/add")
    public String addForm(TagForm form, BindingResult result, Model model){
        if(result.hasErrors())
        {
            model.addAttribute("tag",form);
            return "tag-add";
        }
        Tag t = new Tag();
        if(form.getId() != null)
        	if(tags.findById(form.getId()).isPresent())
        		t = tags.findById(form.getId()).get();
        
        t.setNom(form.getNom());
        tags.save(t);

        return "redirect:/tag/list";
    }
}
