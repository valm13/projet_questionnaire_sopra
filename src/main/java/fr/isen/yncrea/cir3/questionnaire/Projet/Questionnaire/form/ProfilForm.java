package fr.isen.yncrea.cir3.questionnaire.Projet.Questionnaire.form;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfilForm {
    private Long id;
    @NotEmpty
    private String nom;
}
